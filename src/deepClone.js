const mockData = require('./data.json');

/**
 * function used to clone the given object.
 * @param {object} data 
 * @return {object} - cloned object 
 */
const deepClone = data => {

  // check for null/non-object value
  if (data === null || typeof data !== 'object') {
    return data;
  }

  const cloneData = new data.constructor;
  for (let key in data) {
    cloneData[key] = deepClone(data[key]);
  }

  return cloneData;
}

module.exports = {
  deepClone: deepClone
}

// for TDD output
console.log("Object to be clone - ", JSON.stringify(mockData, null, 2));
console.log("---------------");
console.log("Cloned Object - ", JSON.stringify(deepClone(mockData), null, 2));
