
const clientData = require("./clientData.json");
const EARTH_RADIUS = 6371;
const CENTRAL_LOCATION = {
  lat: 51.515419,
  lng: -0.141099
}

/**
 * function to get the company name and it's address withing the given radius (in km).
 * @param {array} clients - array of the clients.
 * @param {number} distanceInKms - distance in km.
 * @return {array} - array of objects containing the companies within a distance
 *  of the 100 km (sorted by company name).
 */
const getAllClient = (clients = [], distanceInKm = 100) => {

  const selectedClients = [];
  clients.forEach(client => {
    client.offices.forEach((office) => {

      const distance = getDistance(office.coordinates.split(","));
      if (distance <= distanceInKm) {
        selectedClients.push({ organization: client.organization, address: office.address });
      }
    });
  });

  // return after sorting the data
  return selectedClients.sort(getSortedResponse('organization'));
}

/**
 * function to calculate the distance (in km) of the given lat lng
 *  from CENTRAL LOCATION lat lng.
 * @param {array} latlong - array containing the lat and long 
 * @return {number} - calculated distance in km.
 */
const getDistance = latlng => {
  const lat2 = latlng[0];
  const lng2 = latlng[1];
  const lat1 = CENTRAL_LOCATION.lat;
  const lng1 = CENTRAL_LOCATION.lng;

  const dLat = (lat2 - lat1) * Math.PI / 180;
  const dLon = (lng2 - lng1) * Math.PI / 180;
  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const dSigma = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return EARTH_RADIUS * dSigma;
}

/**
 * Sorting the data by passing the key 
 * @param {string} sortingKey 
 */
const getSortedResponse = sortingKey => {
  return (first, second) => {
    if (first[sortingKey] > second[sortingKey]) {
      return 1;
    } else if (first[sortingKey] < second[sortingKey]) {
      return -1;
    }
    return 0;
  }
}

module.exports = {
  getAllClient: getAllClient
}

// for TDD output
console.log(JSON.stringify(getAllClient(clientData), null, 2));
