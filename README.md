### Prerequisites
* Node.js - Download and Install [Node.js](http://www.nodejs.org/download/). You can also follow [this gist](https://gist.github.com/isaacs/579814) for a quick and easy way to install Node.js and npm

* NPM - Node.js package manager; should be installed when you install node.js.



### Installation
Install the dependencies and devDependencies
```sh
$ npm install
```
### Question 1 - Deep Clone Object 

Mocked Object
```sh
{
    "filterOptions": {
        "GroupBy": {
            "parameter": "source_field",
            "default": "src-subnetmem",
            "append": false,
            "options": [
                {
                    "label": "Subnet",
                    "default": true,
                    "value": "src-subnetmem",
                    "forceOptions": {
                        "destination_field": "dst-subnetmem",
                        "flow_table_query": "vss-domain-flow-table"
                    }
                },
                {
                    "label": "Zone",
                    "value": "src-zonemem",
                    "forceOptions": {
                        "destination_field": "dst-zonemem",
                        "flow_table_query": "vss-domain-flow-table"
                    }
                }
            ]
        }

    }
}
```

```sh
$ node src/deepClone.js
```

### Question 2 - list of the offices within 100km

Get list of contact partners with offices within 100km of central London (coordinates 51.515419, -0.141099).

You can get list of clients from below mentioned link
### https://success.spidergap.com/partners.json

```sh
$ node src/clients.js
```
Expected Output:-

```sh
[
  {
    "organization": "Blue Square 360",
    "address": "St Saviours Wharf, London SE1 2BE"
  },
  {
    "organization": "Gallus Consulting",
    "address": "Newton House, Northampton Science Park, Moulton Park, Kings Park Road, Northampton, NN3 6LG"
  },
  {
    "organization": "Gallus Consulting",
    "address": "No1 Royal Exchange, London, EC3V 3DG"
  }
]
```

### To run the test cases

```sh
$ npm run test
```
