const mockClients = require('../src/clientData.json');
const { getAllClient } = require("./../src/clients");
const RANGE = 100;

describe("Check Clients", () => {
    let data;
    beforeAll(async () => {
        data = await getAllClient(mockClients, RANGE);
    });

    test("Checking number of clients with in 100 Km list", () => {
        expect(data.length).toEqual(3);
    });

    test("Checking organization name", async () => {
        const [first, second] = data;
        expect(first.organization).toEqual('Blue Square 360');
        expect(second.organization).toEqual('Gallus Consulting');
        expect(second.organization).toEqual('Gallus Consulting');
    });
});
