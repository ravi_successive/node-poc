const { deepClone } = require("./../src/deepClone");
const mockObject = require('../src/data.json');

describe("Check DeepClone", () => {
    test("Checking cloned data", async () => {
        const cloneData = await deepClone(mockObject);
        expect(cloneData).toEqual(mockObject);
    });

    test("Checking cloned data after updating the value", async () => {
        const cloneData = await deepClone(mockObject);
        mockObject.filterOptions.GroupBy.parameter = 'destination_field';
        expect(cloneData).not.toEqual(mockObject);
    })
});
